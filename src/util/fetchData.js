import sampleData from "../sample.json";

export const fetchData = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ data: sampleData });
    }, 100);
  });

export default fetchData;
