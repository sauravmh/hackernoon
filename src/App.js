import { Container, createMuiTheme, ThemeProvider } from "@material-ui/core";
import FooterView from "./components/Footer";
import HeaderView from "./components/Header";
import Post from "./components/Post";

const theme = createMuiTheme({
  palette: {
    type: "light",
    grey: { 900: "#3c3c3a" },
    primary: {
      main: "#00ff01",
    },
    secondary: {
      main: "#3c3c3a",
    },
    tertiary: {
      main: "#CDEDFD",
    },
  },
  Typography: {
    fontFamily: "Roboto",
    fontSize: "18px",
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Container disableGutters maxWidth="xl">
        <HeaderView />
        <Post />
        <FooterView />
      </Container>
    </ThemeProvider>
  );
}

export default App;
