import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  makeStyles,
  Slide,
  useScrollTrigger,
  InputBase,
} from "@material-ui/core";

import { WbSunnySharp, Menu, SearchSharp } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  title: {
    fontFamily: `'IBM Plex Sans', monospace;`,
    fontWeight: "800",
    fontSize: "0.9em",
    color: theme.palette.grey[900],
    letterSpacing: theme.spacing(0.3),
    [theme.breakpoints.up("sm")]: {
      fontSize: "1.8em",
    },
  },
  menuButton: {
    marginRight: theme.spacing(1),
  },
  body1: {
    fontFamily: `'Montserrat', sans-serif`,
    fontWeight: "600",
    fontSize: "0.7em",
    flexGrow: 1,
    color: theme.palette.grey[900],
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  logoImg: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(1),
    maxHeight: 50,
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  grow: {
    flexGrow: 1,
  },
  search: {
    position: "relative",
    color: "white",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.grey[900],
    marginLeft: 0,
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });
  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

function HeaderView({ setDriverView, driverView, props }) {
  const classes = useStyles();
  return (
    <div className={classes.grow}>
      <HideOnScroll {...props}>
        <AppBar>
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <Menu />
            </IconButton>
            <div className={classes.logoImg}>
              <img
                src="https://hackernoon.com/hn-icon.png"
                alt="logo"
                className={classes.logoImg}
              />
            </div>
            <Typography
              variant="h1"
              color="inherit"
              noWrap
              className={classes.title}
            >
              HACKERNOON
            </Typography>
            <div className={classes.grow} />
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchSharp />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ "aria-label": "search" }}
              />
            </div>
            <div>
              <IconButton>
                <WbSunnySharp />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
    </div>
  );
}

export default HeaderView;
