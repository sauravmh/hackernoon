import { Suspense, useState, useEffect } from "react";
import { Grid, Snackbar, IconButton, makeStyles } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { ErrorBoundary } from "react-error-boundary";
import ScaleLoader from "react-spinners/ScaleLoader";

import fetchData from "../../util/fetchData";
import PostView from "./PostView";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: "5em",
  },
}));

function Post() {
  const classes = useStyles();

  const [post, setPost] = useState([]);
  const [isSnackbar, setIsSnackbar] = useState(false);

  useEffect(() => {
    const fetchMarkers = async () => {
      setIsSnackbar(true);
      const response = await fetchData();
      if (response) {
        console.log("[*] User Post", response);
        setPost(response.data);
        setIsSnackbar(false);
      }
    };

    fetchMarkers();
  }, []);

  return (
    <ErrorBoundary FallbackComponent={() => "Loading failed..."}>
      <Grid container justify="center" className={classes.root}>
        <Suspense fallback={<ScaleLoader />}>
          <Grid item xs={8} className={classes.map}>
            <PostView data={post} />
          </Grid>
        </Suspense>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={isSnackbar}
        autoHideDuration={6000}
        onClose={() => setIsSnackbar(false)}
        message="Generating post..."
        action={
          <>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={() => setIsSnackbar(false)}
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          </>
        }
      />
    </ErrorBoundary>
  );
}

export default Post;
