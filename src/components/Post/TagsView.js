import {
  Button,
  Grid,
  makeStyles,
  Step,
  StepLabel,
  Stepper,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
  },
  title: {
    fontFamily: "IBM Plex Mono, monospace",
    fontWeight: 900,
    fontSize: "2em",
  },
  tag: {
    margin: "6px",
  },
}));

function TagsView({ tags }) {
  const classes = useStyles();

  return (
    <Grid container justify="center" className={classes.root} spacing={4}>
      <Grid item xs={12}>
        <Stepper>
          <Step></Step>
          <Step completed>
            <StepLabel>
              <Typography className={classes.title}>TAGS</Typography>
            </StepLabel>
          </Step>
          <Step></Step>
        </Stepper>
      </Grid>
      <Grid item xs={6}>
        <Grid container justify="center">
          {tags && tags.map((tag) => <TagCard tag={tag} classes={classes} />)}
        </Grid>
      </Grid>
    </Grid>
  );
}

const TagCard = ({ tag, classes }) => {
  return (
    <Button variant="contained" color="secondary" className={classes.tag}>
      {`#${tag}`}
    </Button>
  );
};

export default TagsView;
