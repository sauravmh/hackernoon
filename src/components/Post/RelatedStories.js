import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  Link,
  makeStyles,
  Step,
  StepLabel,
  Stepper,
  Typography,
} from "@material-ui/core";
import { Favorite, MoreVert, Share } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    maxHeight: "100vh",
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
  },
  title: {
    fontFamily: "IBM Plex Mono, monospace",
    fontWeight: 900,
    fontSize: "2em",
  },
}));

function RelatedStories({ stories }) {
  const classes = useStyles();

  return (
    <Grid container justify="flex-start" className={classes.root} spacing={4}>
      <Grid item xs={12}>
        <Stepper>
          <Step></Step>
          <Step completed>
            <StepLabel>
              <Typography className={classes.title}>RELATED</Typography>
            </StepLabel>
          </Step>
          <Step></Step>
        </Stepper>
      </Grid>
      <Grid item xs={12}>
        <Grid container justify="space-between" spacing={2}>
          {stories &&
            stories.map((data) => <StoryCard data={data} classes={classes} />)}
        </Grid>
      </Grid>
    </Grid>
  );
}

const StoryCard = ({ classes, data }) => {
  return (
    <Grid item sm={4}>
      <Link
        href={`https://hackernoon.com/${data?.slug || data?.link}`}
        target="_blank"
        rel="noreferrer"
      >
        <Card style={{ maxHeight: "500px" }}>
          <CardHeader
            avatar={
              <Avatar
                aria-label="user"
                className={classes.avatar}
                src={data?.profile?.avatar}
              >
                {data?.companyName || data?.profile?.avatar}
              </Avatar>
            }
            action={
              <IconButton aria-label="settings">
                <MoreVert />
              </IconButton>
            }
            title={data?.companyName || data?.title}
            subheader={data?.website || data?.profile.displayName}
          />
          <CardMedia
            className={classes.media}
            image={data?.image || data?.mainImage}
            title="Blog image"
          />
          <CardContent>
            <Typography variant="body2" color="textSecondary" component="p">
              {data?.text || data?.excerpt}
            </Typography>
          </CardContent>
          <CardActions disableSpacing>
            <IconButton aria-label="add to favorites">
              <Favorite />
            </IconButton>
            <IconButton aria-label="share">
              <Share />
            </IconButton>
          </CardActions>
        </Card>
      </Link>
    </Grid>
  );
};

export default RelatedStories;
