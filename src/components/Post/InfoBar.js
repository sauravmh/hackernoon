import {
  Grid,
  GridList,
  GridListTile,
  makeStyles,
} from "@material-ui/core";
import dateformat from "dateformat";

const useStyles = makeStyles((theme) => ({
  root: {
    maxHeight: 50,
  },
  tile: {
    top: "initial",
    width: "initial",
    position: "initial",
    transform: "initial",
    left: "initial",
    height: "32px",
  },
}));

function InfoBar({ date, reactions, showDate }) {
  const classes = useStyles();
  const alignment = showDate ? "space-between" : "center";
  const iconsAlignment = showDate ? "flex-end" : "center";

  return (
    <Grid container justify={alignment} className={classes.root}>
      {showDate && (
        <Grid item xs={4}>
          {dateformat(date, "dddd, mmmm dS, yyyy")}
        </Grid>
      )}
      <Grid item xs={4}>
        <Grid container justify={iconsAlignment} spacing={2}>
          <Grid item>
            <b>{reactions?.total}</b>
          </Grid>
          <GridList className={classes.gridList} cols={1}>
            <GridListTile
              classes={{
                imgFullWidth: classes.tile,
                imgFullHeight: classes.tile,
              }}
            >
              <img
                src="https://hackernoon.com/emojis/heart.png"
                alt="heart"
                height="32px"
                style={{ marginRight: "0.5em" }}
              />
              <img
                src="https://hackernoon.com/emojis/light.png"
                alt="light"
                height="32px"
                style={{ marginRight: "0.5em" }}
              />
              <img
                src="https://hackernoon.com/emojis/boat.png"
                alt="boat"
                height="32px"
                style={{ marginRight: "0.5em" }}
              />
              <img
                src="https://hackernoon.com/emojis/money.png"
                alt="money"
                height="32px"
                style={{ marginRight: "0.5em" }}
              />
            </GridListTile>
          </GridList>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default InfoBar;
