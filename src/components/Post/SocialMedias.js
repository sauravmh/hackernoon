import {
  Grid,
  GridList,
  GridListTile,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    maxHeight: 50,
  },
  tile: {
    top: "initial",
    width: "initial",
    position: "initial",
    transform: "initial",
    left: "initial",
    height: "24px",
  },
}));

function SocialMedias({ date, reactions, showDate }) {
  const classes = useStyles();

  return (
    <Grid container justify="flex-end" className={classes.root}>
      <Grid item xs={6}>
        <Grid
          container
          justify="space-around"
          spacing={2}
          alignItems="baseline"
        >
          <Grid item>Share this story</Grid>
          <GridList className={classes.gridList} cols={1}>
            <GridListTile
              classes={{
                imgFullWidth: classes.tile,
                imgFullHeight: classes.tile,
              }}
            >
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/twitter-new.png"
                  alt="twitter"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/facebook-new.png"
                  alt="facebook"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/linkedin-new.png"
                  alt="linkedin"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/email-new.png"
                  alt="email"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
            </GridListTile>
          </GridList>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default SocialMedias;
