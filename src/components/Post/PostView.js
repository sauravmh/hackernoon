import { Suspense } from "react";
import {
  Card,
  CardMedia,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";
import ScaleLoader from "react-spinners/ScaleLoader";
import InfoBar from "./InfoBar";
import Profile from "./Profile";
import SocialMedias from "./SocialMedias";
import RelatedStories from "./RelatedStories";
import TagsView from "./TagsView";

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: "3em",
  },
  title: {
    fontFamily: "IBM Plex Mono, monospace",
    fontWeight: "900",
    fontSize: "2em",
  },
  media: {
    height: "657px",
  },
}));

function PostView({ data }) {
  const classes = useStyles();

  return (
    <Grid container justify="center" spacing="4" className={classes.root}>
      <Suspense fallback={<ScaleLoader />}>
        <Grid item xs={12}>
          <Typography align="center" variant="h1" className={classes.title}>
            {data.title}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <InfoBar
            date={data.publishedAt}
            reactions={data.reactions}
            showDate
          />
        </Grid>
        <Grid item xs={12}>
          <Card>
            <CardMedia image={data.mainImage} className={classes.media} />
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Grid container justify="space-between" spacing="2">
            <Grid item xs={2}>
              {data.profile && (
                <Profile
                  handle={data.profile.handle}
                  displayName={data.profile.displayName}
                  bio={data.profile.bio}
                  avatar={data.profile.avatar}
                  github={data.profile.github}
                  twitter={data.profile.twitter}
                />
              )}
            </Grid>
            <Grid item xs={6}>
              <Grid container justify="flex-end" spacing="6">
                <Grid item xs={12}>
                  <ReactMarkdown
                    plugins={[gfm]}
                    children={data.markup}
                    allowDangerousHtml
                  />
                </Grid>
                <Grid item xs={12}>
                  <InfoBar date={data.publishedAt} reactions={data.reactions} />
                </Grid>
                <Grid item xs={6}>
                  <SocialMedias />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3}></Grid>
          </Grid>
        </Grid>
        <Grid xs={12}>
          <RelatedStories stories={data.relatedStories} />
        </Grid>
        <Grid xs={12}>
          <TagsView tags={data.tags} />
        </Grid>
      </Suspense>
    </Grid>
  );
}

export default PostView;
