import {
  Avatar,
  Grid,
  GridList,
  GridListTile,
  makeStyles,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    maxHeight: 50,
  },
  tile: {
    top: "initial",
    width: "initial",
    position: "initial",
    transform: "initial",
    left: "initial",
    height: "24px",
  },
}));

function Profile({ handle, displayName, bio, avatar, github, twitter }) {
  const classes = useStyles();

  return (
    <Grid
      container
      justify="space-between"
      spacing={2}
      className={classes.root}
    >
      <Grid item xs={12}>
        <Avatar variant="square" className={classes.square} src={avatar} />
      </Grid>
      <Grid item xs={12}>
        <Typography>
          <b>@{handle}</b>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography>{displayName}</Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography>{bio}</Typography>
      </Grid>
      <Grid item xs={12}>
        <Grid container justify="flex-start">
          <GridList className={classes.gridList} cols={1}>
            <GridListTile
              classes={{
                imgFullWidth: classes.tile,
                imgFullHeight: classes.tile,
              }}
            >
              <img
                src="https://hackernoon.com/social-icons/twitter-new.png"
                alt="twitter"
                height="24px"
                style={{ marginRight: "0.5em" }}
              />
              <img
                src="https://hackernoon.com/social-icons/github-new.png"
                alt="github"
                height="24px"
                style={{ marginRight: "0.5em" }}
              />
            </GridListTile>
          </GridList>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default Profile;
