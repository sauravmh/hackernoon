import React from "react";
import {
  Grid,
  GridList,
  GridListTile,
  Link,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: theme.palette.primary.main,
    height: "8vh",
    paddingTop: "8px",
    paddingLeft: "1em",
    paddingRight: "1em",
  },
  Typo: {
    color: "secondary",
  },
  tile: {
    top: "initial",
    width: "initial",
    position: "initial",
    transform: "initial",
    left: "initial",
    height: "32px",
  },
  footerIcons: {
    maxHeight: "3vh",
  },
  footerLinks: {
    marginRight: "8px",
    color: "black",
    fontWeight: 500,
  },
}));

function FooterView() {
  const classes = useStyles();

  return (
    <Grid
      container
      className={classes.footer}
      justify="space-between"
      alignItems="center"
    >
      <Grid item md="9">
        <Typography>
          <Link
            href="https://blog.sauravmh.com"
            className={classes.footerLinks}
          >
            About us
          </Link>
          <Link
            href="https://blog.sauravmh.com"
            className={classes.footerLinks}
          >
            Contact
          </Link>
          <Link
            href="https://blog.sauravmh.com"
            className={classes.footerLinks}
          >
            Careers
          </Link>
          <Link
            href="https://blog.sauravmh.com"
            className={classes.footerLinks}
          >
            Terms and Conditions
          </Link>
        </Typography>
      </Grid>
      <Grid item md="3">
        <Grid container justify="flex-end">
          <GridList className={classes.gridList} cols={1}>
            <GridListTile
              classes={{
                imgFullWidth: classes.tile,
                imgFullHeight: classes.tile,
              }}
              className={classes.footerIcons}
            >
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/twitter-new.png"
                  alt="twitter"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/facebook-new.png"
                  alt="facebook"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/linkedin-new.png"
                  alt="linkedin"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
              <a href="https://blog.sauravmh.com">
                <img
                  src="https://hackernoon.com/social-icons/email-new.png"
                  alt="email"
                  height="24px"
                  style={{ marginRight: "0.5em" }}
                />
              </a>
            </GridListTile>
          </GridList>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default FooterView;
